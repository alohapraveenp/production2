<!DOCTYPE html>
<html>
<title>Team Aloha</title>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</style>
</head>
<body>
<div class='container'>


<?php
require_once("conf/conf.mysql.inc.php");

echo "<h2>Fetching database from $servername</h2>";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT restaurant, city, title, country FROM restaurant";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    echo " <div class='table-responsive'> <table class='table table-striped table-bordered'><thead><tr><th>Restaurant</th><th>City</th><th>Title</th><th>Country</th></tr></thead>";
    while($row = $result->fetch_assoc()) {
         echo "<tr><td>". $row["restaurant"]. "</td><td>". $row["city"]. "</td><td> " . $row["title"]. "</td><td>" . $row["country"]. "</td></tr>";
        #echo "<tab3><b>Restaurant:</b></tab3>"  . $row["restaurant"]. "<tab3><b>City:</b></tab3> " . $row["city"]. "<tab3><b>Title</b></tab3> " . $row["title"]. "<br>";

    }
    echo "</table> </div> ";
} else {
    echo "0 results";
}
$conn->close();
?>
</div>
</body>
</html>

