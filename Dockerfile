FROM 004941777231.dkr.ecr.ap-southeast-2.amazonaws.com/parent:latest

# Install app
RUN rm -rf /var/www/html/*
ADD src /var/www/html

RUN rm -rf /var/www/html/conf
#ADD src/conf /etc/PHP_LIB/conf

#RUN sed -i.bak 's/short_open_tag = Off/short_open_tag = On/' /etc/php/7.0/apache2/php.ini
ADD src/conf/conf.mysql_prod2.inc.php /var/www/html/conf/conf.mysql.inc.php


#COPY src/ /var/www/html/

